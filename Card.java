public class Card {
	
	//Fields
	private String suit;
	private String rank;
	
	//cosntructor
	public Card (String suit, String rank) {
		this.suit = suit;
		this.rank = rank;
	}
	
	//getters
	public String getSuit() {
		return this.suit;
	}
	public String getRank() {
		return this.rank;
	}
	
	//toString methods
	public String toString() {
		return this.rank + " of " + this.suit;
	}
	
	//calculateScore Method
	public double calculateScore() {
		String[] rank = new String[]{"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		String[] suit = new String[]{"Spades", "Clubs", "Diamonds", "Hearts"};
		double cardScore = 0.0;
		
		for(int i = 0; i < rank.length; i++) {
			if(rank[i].equals(this.rank)) {
				cardScore += i + 1;
				break;
			}
		}
		for(int i = 0; i < suit.length; i++) {
			if(suit[i].equals(this.suit)) {
				cardScore += (Double.valueOf(i)/10) + 0.1;
				break;
			}
		}
		return cardScore;
	}
}