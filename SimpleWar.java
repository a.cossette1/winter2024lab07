public class SimpleWar {
	public static void main(String[] args) {
		
		Deck deck = new Deck();
		deck.shuffle();
		
		//Player Rounds Won Score
		int playerOneRounds = 0;
		int playerTwoRounds = 0;
		
		while(deck.length() > 1){
			//Player Cards
			Card playerOneCard = deck.drawTopCard();
			Card playerTwoCard = deck.drawTopCard();
			
			//Player Point Variables
			double playerOnePoints = playerOneCard.calculateScore();
			double playerTwoPoints = playerTwoCard.calculateScore();
			
			System.out.println("Your Card is: " + playerOneCard + " Your score is: " + playerOnePoints);
			System.out.println("Your Card is: " + playerTwoCard + " Your score is: " + playerTwoPoints);
			
			//Round Winner statements
			if(playerOnePoints > playerTwoPoints) {
				System.out.println("\n" + "Player One wins with a score of: " + playerOnePoints);
				playerOneRounds += 1;
			}
			else {
				System.out.println("\n" + "Player Two wins with a score of: " + playerTwoPoints);
				playerTwoRounds += 1;
			}
			
			System.out.println("\n" + "Total Score:");
			System.out.println("Player One:" + playerOneRounds);
			System.out.println("Player Two:" + playerTwoRounds);
		}
		
		//Winner Message
		if(playerOneRounds > playerTwoRounds) {
			System.out.println("Congratulations Player One you have won the game!");
		}
		else if(playerTwoRounds > playerOneRounds) {
			System.out.println("Congratulations Player Two you have won the game!");
		}
		else{
			System.out.println("Draw... Try again!");
		}
	}
}