import java.util.Random;

public class Deck {
	
	//fields
	private Card[] cards;
	private int numberOfCards;

	//random
	Random rng;

	//constructor
	public Deck() {
		this.numberOfCards = 52;
		rng = new Random();
		this.cards = new Card[this.numberOfCards];
		String[] suit = new String[]{"Hearts", "Spades", "Clubs", "Diamonds"};
		String[] rank = new String[]{"Ace", "Two", "Three", "Four", "Five", "Six", "Seven", "Eight", "Nine", "Ten", "Jack", "Queen", "King"};
		
		//loop for Card object
		for(int i = 0; i < this.cards.length; i++) {
			this.cards[i] = new Card(suit[i/rank.length], rank[i%rank.length]);
		}
	}
	
	//length method
	public int length() {
		return this.numberOfCards;
	}
	
	//drawTopCard method 
	public Card drawTopCard() {
		this.numberOfCards--;
		return this.cards[numberOfCards];
	}
	
	//toString method
	public String toString() {
		String listOfCards = "";
		
		for(int i = 0; i < this.cards.length; i++) {
			listOfCards += cards[i] + " \n";
		}
		
		return listOfCards;
	}
	
	//shuffle method
	public void shuffle() {
		for(int i = 0; i < this.cards.length; i++) {
			int number = this.rng.nextInt(this.cards.length);
			Card newCard = cards[i];
			cards[i] = cards[number];
			cards[number] = newCard;
		}
	}
}